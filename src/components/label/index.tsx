import React, { ReactElement } from "react"
import { Text } from "@chakra-ui/react"
export const TextLabel = (text: string): ReactElement => {
  return (
    <Text as="span">
      {text}
    </Text>
  )
}