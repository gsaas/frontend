import React, { ReactElement } from "react"
import { Input } from "@chakra-ui/react"
export interface ITextfieldProps {
  value: string;
  update: (v: string) => void;
  placeholder?: string;
  variant?: "outline" | "unstyled" | "filled" | "flushed"
}
export const Textfield = ({
  value,
  update,
  placeholder = "",
  variant = "outline"
}: ITextfieldProps): ReactElement => {
  return (
    <Input
      value={value}
      onChange={e => update(e.target.value)}
      placeholder={placeholder}
      variant={variant}
      size="md"
    />
  )
}
