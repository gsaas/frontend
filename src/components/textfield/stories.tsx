import React from "react"
import { Story, Meta } from "@storybook/react"
import { Textfield, ITextfieldProps } from "."
export default {
  title: "Components/Textfield",
  component: Textfield
} as Meta
const Template: Story<ITextfieldProps> = (args) => <Textfield {...args} />
const WrappedTemplate: Story<ITextfieldProps> = (args) => (
  <div style={{margin:"1rem", padding: "25px", border:"1px solid red"}}>
    <Textfield {...args} />
  </div>
)
export const Default = Template.bind({})
Default.args = {
  value: "",
  update: () => { console.log("Update called") }
}
export const WithPlaceholder = Template.bind({})
WithPlaceholder.args = {
  ...Default.args,
  placeholder: "enter text"
}
export const WithPlaceholderFilled = Template.bind({})
WithPlaceholderFilled.args = {
  ...WithPlaceholder.args,
  variant: "filled"
}
export const WithUndefinedVariant = Template.bind({})
WithUndefinedVariant.args = {
  ...Default.args,
  variant: undefined
}
export const UnstyledTextField = WrappedTemplate.bind({})
/**
 * has placeholder content
 */
UnstyledTextField.args = {
  ...Default.args,
  placeholder: "Some placeholder content",
  variant: "unstyled"
}
