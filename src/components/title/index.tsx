import { FlexProps, Text } from "@chakra-ui/react"
import React, { ReactElement } from "react"
import { ITitleProps } from "./interface"
export const Title = ({
  text,
  size
}: ITitleProps & FlexProps): ReactElement => {
  return (
    <Text as={size}>{ text }</Text>
  )
}