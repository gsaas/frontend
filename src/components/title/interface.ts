export interface ITitleProps {
    text: string,
    size: THtmlTitle
}
type THtmlTitle = "h1" | "h2" | "h3" | "h4" | "h5"
