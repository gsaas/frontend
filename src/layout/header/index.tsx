import { Divider, Flex, Spacer, Text } from "@chakra-ui/react"
import React, { ReactElement } from "react"
export const Header = (): ReactElement => {
  return (
    <Flex as="header" direction="column" p="1.5">
      <Divider orientation="horizontal" width="100%" height="2px" bg="teal" m="0.5"/>
      <Flex direction="row" m="0.5" align="center" justify="center">
        <Text as="h2" flexWrap="wrap">
        GSaaS - Game-Server as a Service
        </Text>
        <Spacer/>
        <Flex
          m="0.5"
          align="center"
          justify="center"
          direction="row"
          minW="35%"
        >
          <Text
            as="button" 
            p="1"
          >
          Product
          </Text>
          <Divider orientation="vertical" height="20px" bg="gray" m="2px" width="2px"/>
          <Text
            as="button" 
            p="0.5"
          >
          Platform
          </Text>
          <Spacer/>
          <Text
            as="button"
            bg="lightgrey"
            borderRadius="18%"
            p="0.5"
            variant="outline"
          >
        Login
          </Text>
        </Flex>
      </Flex>
      <Divider orientation="horizontal" width="100%" height="2px" bg="teal" m="0.5"/>
    </Flex>
  )
}