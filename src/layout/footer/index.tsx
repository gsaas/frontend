import { Flex, Spacer, Text } from "@chakra-ui/react"
import React, { ReactElement } from "react"
export const Footer = (): ReactElement => {
  return (
    <Flex as="footer" direction="row">
      <Text as="h2" flexWrap="wrap">
        Footer Dummy
      </Text>
      <Spacer/>
      <Text as="span">
        Impressum
      </Text>
    </Flex>
  )
}