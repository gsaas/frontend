import { Story } from "@storybook/react"
import React from "react"
import { LayoutContainer } from "./index"
import { IContainerProps } from "./interface"
export default {
  title: "Layout/LayoutContainer",
  component: LayoutContainer
}
const Template: Story<IContainerProps> = (args) => <LayoutContainer {...args}/>
export const Default = Template.bind({})
Default.args = {
  content: (
    <div>
      Some label within own Box
    </div>
  )
}