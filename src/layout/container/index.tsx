import { Flex, FlexProps } from "@chakra-ui/react"
import React from "react"
import { ReactElement } from "react"
import { IContainerProps } from "./interface"
export const LayoutContainer = (
  {
    content
  }: IContainerProps & FlexProps
): ReactElement => {
  return (
    <Flex direction="column">
      {content}
    </Flex>
  )
}