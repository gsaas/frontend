import { ReactElement } from "react"
export interface IContainerProps {
  content?: ReactElement
}