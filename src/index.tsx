import React from "react"
import ReactDom from "react-dom"
import { Textfield } from "./components/textfield"
import { ChakraProvider } from "@chakra-ui/react"
import theme from "@chakra-ui/theme"
ReactDom.render(
  <ChakraProvider theme={theme}>
    <Textfield
      value=""
      update={ () => {
        console.log("Update")
      }}
      placeholder="Placeholder"
    />
  </ChakraProvider>,
  document.getElementById("root")
)