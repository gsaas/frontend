/**
 * matchExact returns true if the given string exactly matches
 * the given regex. If orEmpty is true, matchExact will return 
 * true if the string is empty.
 * @param regex The regex to test
 * @param str The string to test
 * @param orEmpty return true on empty string
 * @returns true if the string matches the regex, false otherwise.
 */
export const matchExact = (regex: RegExp, str: string, orEmpty = false): boolean => {
  if (orEmpty && str === "") {
    return true
  }
  const match = str.match(regex)
  return match !== null && str === match[0]
}
/**
 * A regex to match letters => [a-zA-Z]+
 */
export const letterOnlyRegex = new RegExp(/[a-zA-Z]+/g)
/**
 * Check if the given string only contains letters.
 * @param str string to test
 * @returns true if the string only contains letters, false otherwise.
 */
export const isLetters = (str: string,): boolean => matchExact(letterOnlyRegex, str)
/**
 * A regex to match numbers => [0-9]+
 */
export const numberOnlyRegex = new RegExp(/[0-9]+/g)
/**
 * Check if the given string only contains numbers.
 * @param str string to test
 * @returns true if the string only contains letters, false otherwise.
 */
export const isNumbers = (str: string): boolean => matchExact(numberOnlyRegex, str)
/**
 * A regex to match an E-Mail address => ^[^\s@]+@[^\s@]+\.[^\s@]+$
 * @see https://ui.dev/validate-email-address-javascript/
 */
export const emailRegex = new RegExp(/^[^\s@]+@[^\s@]+\.[^\s@]+$/g)
/**
 * Check if the given string is a valid E-Mail address.
 * @see https://ui.dev/validate-email-address-javascript/
 * @param str string to test
 * @returns true if the string is a valid e-mail, false otherwise.
 */
export const isEmail = (str: string): boolean => matchExact(emailRegex, str)
