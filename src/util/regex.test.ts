import { matchExact, isNumbers, isLetters, isEmail } from "./regex"
const r1 = new RegExp(/[a-zA-Z]+/g)
const r2 = new RegExp(/Hello/g)
test("regex to match", () => {
  expect(matchExact(r1, "aaBB")).toBe(true)
})
test("regex to match", () => {
  expect(matchExact(r2, "Hello")).toBe(true)
})
test("empty option to work", () => {
  expect(matchExact(r1, "", true)).toBe(true)
})
test("regex to not match empty string", () => {
  expect(matchExact(r2, "", false)).toBe(false)
})
test("isLettersOnly to work", () => {
  expect(isLetters("foobar")).toBe(true)
})
test("isNumbersOnly to work", () => {
  expect(isNumbers("1234")).toBe(true)
})
test("isLettersOnly to fail on empty string", () => {
  expect(isLetters("")).toBe(false)
})
test("isNumbersOnly to fail on empty string", () => {
  expect(isNumbers("")).toBe(false)
})
test("isEmail to work", () => {
  expect(isEmail("foo@bar.dev")).toBe(true)
})
test("isEmail to fail on empty string", () => {
  expect(isEmail("")).toBe(false)
})