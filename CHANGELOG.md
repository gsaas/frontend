# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### 0.0.1 (2021-07-06)


### Others

* **project-configuration:** add eslint, change storybook config ([4361ffc](https://gitlab.com/gsaas/frontend/commit/4361ffc958bcbc04d4b802c1ad04a3bc8af757f6))
* **project-configuration:** add standard-version and commitizen to project ([6beb7ef](https://gitlab.com/gsaas/frontend/commit/6beb7ef0cd9d0e3433b9da01ee7f85af14d24533)), closes [#1](https://gitlab.com/gsaas/frontend/issues/1)
